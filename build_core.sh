#!/bin/bash

RED='\033[0;31m'
GREEN='\033[0;32m'
NC='\033[0m'

function quit {
    echo -e "${RED}Task failed. $1 ${NC}"
    exit 1
}

function showtitle {
    echo -e "\n\n${GREEN}--- $1 ...${NC}\n"
}

# -------- install golang ---------------
showtitle "Installing golang"

if [[ "$OSTYPE" == "linux-gnu"* ]]; then
        GO_VERSION=go1.22.2.linux-amd64
        EXPECTED_FP=5901c52b7a78002aeff14a21f93e0f064f74ce1360fce51c6ee68cd471216a17
elif [[ "$OSTYPE" == "darwin"* ]]; then
        # Mac OSX
        ARCH=`uname -a | rev | cut -d " " -f 1 | rev`
        if [[ "$ARCH" = "x86_64" ]]; then
            GO_VERSION=go1.22.2.darwin-amd64
            EXPECTED_FP=33e7f63077b1c5bce4f1ecadd4d990cf229667c40bfb00686990c950911b7ab7
        else
            GO_VERSION=go1.22.2.darwin-arm64
            EXPECTED_FP=660298be38648723e783ba0398e90431de1cb288c637880cdb124f39bd977f0d
        fi
else
        echo "$OSTYPE is currently not supported."
        exit 1
fi


if [[ $(ls -A ${GO_VERSION}.tar.gz) ]]
then
    echo "Reusing downloaded golang bundle"
else
    echo "Installing go lang bundle ${GO_VERSION}.tar.gz from https://golang.org/dl/$GO_VERSION.tar.gz"
    curl -L -o "${GO_VERSION}.tar.gz" "https://golang.org/dl/${GO_VERSION}.tar.gz"
    if [[ "$OSTYPE" == "linux-gnu"* ]]; then
      ACTUAL_FP=`sha256sum $GO_VERSION.tar.gz | cut -d " " -f1`
    else
      ACTUAL_FP=`shasum -a 256 $GO_VERSION.tar.gz | cut -d " " -f1`
    fi

    if [[ ! $ACTUAL_FP == $EXPECTED_FP ]]
    then
        quit "Download seems to be corrupted. Cancelling build."
    fi
fi

if [[ -d ./golang ]]
then
    if [[ $(ls -A ./golang/*) ]]
    then
        rm -r ./golang/*
    fi
else
    mkdir ./golang
fi
tar -C ./golang -xzf $GO_VERSION.tar.gz || quit "Could not untar $GO_VERSION.tar.gz"


# -------- update submodules ---------------
showtitle "Updating submodules"
git submodule sync --recursive || quit "git submodule sync --recursive"
git submodule update --init --recursive || quit "git submodule update --init --recursive"


# -------- init environment variables ---------------
cd ./golang
export GOPATH=`pwd`
export GO_LANG=`pwd`/go/bin
export GO_COMPILED=`pwd`/bin
export PATH="${GO_LANG}:${GO_COMPILED}:${PATH}"
go env -w GOFLAGS=-mod=
go env
cd ..

# -------- init gomobile ---------------
showtitle "Getting gomobile"
./golang/go/bin/go install golang.org/x/mobile/cmd/gomobile || quit "./golang/go/bin/go install golang.org/x/mobile/cmd/gomobile"

showtitle "initiating gomobile"
./golang/bin/gomobile init || quit "./golang/bin/gomobile init"


# -------- prepare snowflake ---------------
showtitle "Checking out snowflake repository"
cd ./IPtProxy/snowflake
git checkout --force --quiet 05a95802c195b1d8a68bb6fe4fa98f12763af519 || quit "Failed to checkout snowflake at commit b130151b24db155e4ea89517b72b73b21949c84b (v2.9.2)"
git submodule update --init --recursive || quit "Failed to update snowflake submodules"
cd ..

showtitle "Applying patches to snowflake"
patch --directory=snowflake --strip=1 < snowflake.patch || quit 
cd ..

showtitle "Applying patch to IPtProxy"
patch --directory=IPtProxy --strip=1 < iptproxy.patch || quit

# -------- build core ---------------

if [[ -d ./lib ]]
then
    if [[ $(ls -A ./lib/*) ]]
    then
        rm -r ./lib/*
    fi
else
    mkdir ./lib
fi


showtitle "Building Bitmask core as full aar"
gomobile bind -x -target android -ldflags="-s -w" -tags=netcgo -androidapi=21 -v -trimpath -o lib/bitmaskcore.aar ./obfsvpn/client/ ./motd ./IPtProxy/IPtProxy.go

showtitle "Building Bitmask web core as full aar including pgpverify"
gomobile bind -x -target='android' -ldflags="-s -w" -androidapi=21 -v -tags=netcgo -trimpath -o ./lib/bitmaskcore_web.aar -v ./pgpverify ./obfsvpn/client ./IPtProxy/IPtProxy.go ./motd

showtitle "Building Bitmask core for armv7"
gomobile bind -target='android/arm' -ldflags="-s -w" -androidapi=21 -v -tags=netcgo -trimpath -o ./lib/bitmaskcore_arm.aar -v ./obfsvpn/client ./IPtProxy/IPtProxy.go ./motd

showtitle "Building Bitmask core for x86"
gomobile bind -target='android/386' -ldflags="-s -w" -androidapi=21 -v -tags=netcgo -trimpath -o ./lib/bitmaskcore_x86.aar -v ./obfsvpn/client ./IPtProxy/IPtProxy.go ./motd

showtitle "Building Bitmask core for arm64"
gomobile bind -target='android/arm64' -ldflags="-s -w" -androidapi=21 -v -tags=netcgo -trimpath -o ./lib/bitmaskcore_arm64.aar -v ./obfsvpn/client ./IPtProxy/IPtProxy.go ./motd

showtitle "Building Bitmask core for x86_64"
gomobile bind -target='android/amd64' -ldflags="-s -w" -androidapi=21 -v -tags=netcgo -trimpath -o ./lib/bitmaskcore_x86_64.aar -v ./obfsvpn/client ./IPtProxy/IPtProxy.go ./motd
