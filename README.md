# DEPRECATED

This repository only exists for compatibility reasons with existing clients depending on it. 
The repository has been rennamed to Bitmask Core Android and can be found here: https://0xacab.org/leap/android_libs/bitmask-core-android.

All further development will happen in the aforementioned repo.

### Bitmask Core 

This repository contains all golang dependencies currently used in Bitmask Android and custom branded versions of it:
* obfsvpn: a library containing different obfs4 based censorship circumvention transports
* IPtProxy: a library wrapping snowflake client and snowflake proxy, forked from https://github.com/tladesignz/IPtProxy and adapted for our purposes
* pgpverify: a small library for verifying the integrity of downloaded app updates (only used in apks distributed from a website)
* motd: Message of the day library, shared between Bitmask Desktop and Mobile
* more shared code between Bitmask Desktop and Mobile clients **(tbd.)**

Run the follwing command to build Bitmask Core for all Android build flavors (runs currently only on Linux 64-bit distros):

`./build_core.sh`

## Adding new go libraries

1. In order to add new library we currently add them as git submodules. 
```
git submodule add git://<repository_url.git> <new_lib_directory>
```

2. Add the new go module to the go workspace.
```
go work use <new_lib_directory>
```

3. Make sure the `bitmaskcore/golang` directory is empty.
```
# you should be in bitmaskcore
sudo rm -rf ./golang/*
```
4. And let go add any missing modules.
```
# you should be in bitmaskcore
go mod tidy
```
5. extend the `gomobile bind` statements with the new module in `./build_core.sh` and `./build_core_dev.sh`
6. test the changes with running `./build_core_dev.sh`
7. `git add` your changes and commit them
8. run `./build_core.sh` and make sure it succeeds
